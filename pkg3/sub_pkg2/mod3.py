def baz():
    print("[mod3] baz()")


class Baz:
    pass


from pkg3.sub_pkg1.mod1 import foo
foo()

from ..sub_pkg1 import mod2
mod2.bar()