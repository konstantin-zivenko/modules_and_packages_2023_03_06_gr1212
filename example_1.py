s = "Modules and packages"
a = [200, 300, 400]


def func(arg):
    print(f"arg = {arg}")


class Foo:
    pass

# print(__name__)

# print(s)
# x = Foo()
# print(x)

if __name__ == "__main__":
    print(s)
    x = Foo()
    print(x)
